<?php

namespace App\Presenters;

use App\Components\SignInFormFactory;
use App\Components\SignUPFormFactory;
use Nette\Application\UI\Form;


class SignPresenter extends BasePresenter
{
	/** @var Components\SignInFormFactory */
	private $signInFactory;

	/** @var Components\SignUpFormFactory */
	private $signUpFactory;


	public function __construct(SignInFormFactory $signInFactory, SignUpFormFactory $signUpFactory)
	{
		$this->signInFactory = $signInFactory;
		$this->signUpFactory = $signUpFactory;
	}


	/**
	 * Sign-in form factory.
	 * @return Form
	 */
	protected function createComponentSignInForm()
	{
		return $this->signInFactory->create(function () {
			$this->redirect('Homepage:');
		});
	}

	/**
	 * Sign-up form factory.
	 * @return Form
	 */
	protected function createComponentSignUpForm()
	{
		return $this->signUpFactory->create(function () {
			$this->redirect('Homepage:');
		});
	}


	public function actionOut()
	{
		$this->getUser()->logout();
                                   $this->redirect('Homepage:');
	}
}
